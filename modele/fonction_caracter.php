<?php

  function insert_new_caracter($post){
    //ajoute un nouveau joueurs
    include 'bdd.php';
    $nom = $post["nom"];
    $bonus_initiative = $post["bonus_initiative"];
    $initiative = $post["initiative"];
    $CA = $post["CA"];
    $vigueur = $post["vigueur"];
    $volonte = $post["volonte"];
    $reflexes = $post["reflexes"];
    $pv_actuel = $post["pv_actuel"];
    $pv_max = $post["pv_max"];

    $sql = "INSERT INTO joueurs
            VALUES (null,:nom,:bonus_initiative,:CA,:vigueur,:volonte,:reflexes,:pv_actuel,:pv_max,:initiative);";
    $req = $pdo->prepare($sql);

    //youpi ! tout un tas d'instructions pour associer les valeurs aux paramètres
    $req->bindValue('nom', $nom, PDO::PARAM_STR);
    $req->bindValue('bonus_initiative', $bonus_initiative, PDO::PARAM_INT);
    $req->bindValue('CA', $CA, PDO::PARAM_INT);
    $req->bindValue('vigueur', $vigueur, PDO::PARAM_INT);
    $req->bindValue('volonte', $volonte, PDO::PARAM_INT);
    $req->bindValue('reflexes', $reflexes, PDO::PARAM_INT);
    $req->bindValue('pv_actuel', $pv_actuel, PDO::PARAM_INT);
    $req->bindValue('pv_max', $pv_max, PDO::PARAM_INT);
    $req->bindValue('initiative', $initiative, PDO::PARAM_INT);
    $req->execute();

    return $req;
  }

  function select_caracter(){
    //renvoie tous les joueurs
    include 'bdd.php';
    $req = $pdo->prepare("SELECT *  from joueurs;");
    $req -> execute();
    return $req;
  }

  function delete_caracter($id){
    //supprime un joueurs et vide en conséquence la table participants
    include 'bdd.php';
    include 'fonction_participate.php';
    delete_participant_caracter($id);
    $req = $pdo->prepare("DELETE FROM joueurs WHERE id = :id;");
    $req->bindParam('id', $id, PDO::PARAM_INT);
    $req->execute();
    return $req;
  }

  function get_caracter($id){
    //renvoie un joueurs
    include 'bdd.php';
    $req = $pdo->prepare("SELECT * FROM joueurs WHERE id = :id;");
    $req->bindParam('id', $id, PDO::PARAM_INT);
    $req->execute();
    return $req->fetch();
  }

  function update_caracter($post){
    //met à jour un joueurs
    include 'bdd.php';
    $nom = $post["nom"];
    $bonus_initiative = $post["bonus_initiative"];
    $initiative = $post["initiative"];
    $CA = $post["CA"];
    $vigueur = $post["vigueur"];
    $volonte = $post["volonte"];
    $reflexes = $post["reflexes"];
    $pv_actuel = $post["pv_actuel"];
    $pv_max = $post["pv_max"];
    $id = $post["id"];

    $sql = "UPDATE joueurs SET
            nom = :nom,
            bonus_initiative = :bonus_initiative,
            CA = :CA,
            vigueur = :vigueur,
            volonte = :volonte,
            reflexes = :reflexes,
            PV_actuel = :pv_actuel,
            PV_max = :pv_max,
            initiative = :initiative
            WHERE id = :id;";
    $req = $pdo->prepare($sql);

    //youpi ! tout un tas d'instructions pour associer les valeurs aux paramètres et oui encore ce commentaire
    $req->bindValue('nom', $nom, PDO::PARAM_STR);
    $req->bindValue('bonus_initiative', $bonus_initiative, PDO::PARAM_INT);
    $req->bindValue('initiative', $initiative, PDO::PARAM_INT);
    $req->bindValue('CA', $CA, PDO::PARAM_INT);
    $req->bindValue('vigueur', $vigueur, PDO::PARAM_INT);
    $req->bindValue('volonte', $volonte, PDO::PARAM_INT);
    $req->bindValue('reflexes', $reflexes, PDO::PARAM_INT);
    $req->bindValue('pv_actuel', $pv_actuel, PDO::PARAM_INT);
    $req->bindValue('pv_max', $pv_max, PDO::PARAM_INT);
    $req->bindValue('id', $id, PDO::PARAM_INT);
    return $req->execute();
  }
  function update_caracter_pv_initiative($pv_actuel, $id, $initiative){
    //met à jour les pv_actuel d'un joueurs
    include 'bdd.php';

    $sql = "UPDATE joueurs SET
            PV_actuel = :pv_actuel,
            initiative = :initiative
            WHERE id = :id;";
    $req = $pdo->prepare($sql);

    $req->bindValue('pv_actuel', $pv_actuel, PDO::PARAM_INT);
    $req->bindValue('initiative', $init, PDO::PARAM_INT);
    $req->bindValue('id', $id, PDO::PARAM_INT);
    return $req->execute();
  }

 ?>
