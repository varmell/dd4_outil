<?php

  function insert_new_monster($post){
    //ajoute un nouveau monstre
    include 'bdd.php';
    $id_combats = $post["id_combats"];
    $id_modeles = $post["id_modeles"];
    $pv_actuel = $post["pv_actuel"];
    $divers = $post["divers"];
    $initiative = $post["initiative"];

    $sql = "INSERT INTO monstres
            VALUES (null,:id_combats,:id_modeles,:pv_actuel,:divers,:initiative);";
    $req = $pdo->prepare($sql);

    $req->bindValue('id_combats', $id_combats, PDO::PARAM_INT);
    $req->bindValue('id_modeles', $id_modeles, PDO::PARAM_INT);
    $req->bindValue('pv_actuel', $pv_actuel, PDO::PARAM_INT);
    $req->bindValue('divers', $divers, PDO::PARAM_STR);
    $req->bindValue('initiative', $initiative, PDO::PARAM_INT);
    $req->execute();

    return $req;
  }

  function select_monstrer(){
    //renvoie tous les monstres
    include 'bdd.php';
    $req = $pdo->prepare("SELECT *  from monstres;");
    $req -> execute();
    return $req;
  }

  function select_monster_fight($id_combats){
    //renvoie tous les monstres d'un combat
    include 'bdd.php';
    $req = $pdo->prepare("SELECT *  from monstres where id_combats = :id_combats;");
    $req->bindValue('id_combats', $id_combats, PDO::PARAM_INT);
    $req -> execute();
    return $req;
  }

  function delete_monster($id, $id_combats, $id_modeles){
    //supprime un monstre
    include 'bdd.php';
    $req = $pdo->prepare("DELETE FROM monstres WHERE id = :id and id_combats = :id_combats and id_modeles = :id_modeles;");
    $req->bindParam('id', $id, PDO::PARAM_INT);
    $req->bindValue('id_combats', $id_combats, PDO::PARAM_INT);
    $req->bindValue('id_modeles', $id_modeles, PDO::PARAM_INT);
    $req->execute();
    return $req;
  }
  function delete_monster_fight($id_combats){
    //supprime un monstre
    include 'bdd.php';
    $req = $pdo->prepare("DELETE FROM monstres WHERE id_combats = :id_combats;");
    $req->bindValue('id_combats', $id_combats, PDO::PARAM_INT);
    $req->execute();
    return $req;
  }

  function get_monster($id, $id_combats, $id_modeles){
    //renvoie un monstre
    include 'bdd.php';
    $req = $pdo->prepare("SELECT * FROM monstres WHERE id = :id and id_combats = :id_combats and id_modeles = :id_modeles;");
    $req->bindParam('id', $id, PDO::PARAM_INT);
    $req->bindValue('id_combats', $id_combats, PDO::PARAM_INT);
    $req->bindValue('id_modeles', $id_modeles, PDO::PARAM_INT);
    $req->execute();
    return $req->fetch();
  }

  function update_monstre($pv_actuel, $divers, $id){
    //met à jour les PV_actuel et le champ divers d'un monstre
    include 'bdd.php';

    $sql = "UPDATE monstres SET
            pv_actuel = :pv_actuel,
            divers = :divers
            WHERE id = :id;";
    $req = $pdo->prepare($sql);

    $req->bindValue('pv_actuel', $pv_actuel, PDO::PARAM_INT);
    $req->bindValue('divers', $divers, PDO::PARAM_STR);
    $req->bindValue('id', $id, PDO::PARAM_INT);

    return $req->execute();
  }
  function update_initiative_monstre($id, $initiative){
    include 'bdd.php';

    $sql = "UPDATE monstres SET
            initiative = :initiative
            WHERE id = :id;";
    $req = $pdo->prepare($sql);
    $req->bindValue('initiative', $initiative, PDO::PARAM_INT);
    $req->bindValue('id', $id, PDO::PARAM_INT);

    return $req->execute();
  }

 ?>
