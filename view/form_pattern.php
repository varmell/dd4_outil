<html>
  <head>
    <?php include 'config.php'; ?>
    <link rel="icon" type="image/png" href="<?php echo $domaine; ?>ressources/logo.png" />
    <title>DD4-Formulaire d'un modèle</title>
  </head>
  <body>
    <div>
      <?php
      include 'header.php';
      include '../modele/fonction_pattern.php';
      include 'nav.php';
      ?>
    </div>
    <?php
      $action = $_GET["action"];
      if($action=="edition")
        $pattern = get_pattern($_GET["id"]);
    ?>
    <div id="studio_combat">
      <center><h2>Modèle de monstres</h2></center>
      <form action="<?php echo $domaine ?>controller/pattern_studio_controller.php?action=<?php echo $action; ?>" method="post">
        <table border="0" width="500" align="center">
          <tr>
            <?php if($action=="edition") {?><td width="300"><b>Id</b></td><?php } ?>
            <td width="300">
              <input type="<?php if($action=="nouveau") echo "hidden"; else echo "text"; ?>" name="id" value="<?php if($action=="edition") echo $pattern["id"]; ?>">
            </td>
          </tr>
          <tr>
            <td width="300"><b>Nom :</b></td>
            <td width="300">
              <input type="text" name="nom" value="<?php if($action=="edition") echo $pattern["nom"]; ?>"/>
            </td>
          </tr>
          <tr>
            <td width="300"><b>Bonus d'initiative :</b></td>
            <td width="300">
              <input type="text" name="initiative" value="<?php if($action=="edition") echo $pattern["bonus_initiative"]; ?>">
            </td>
          </tr>
          <tr>
            <td width="300"><b>CA :</b></td>
            <td width="300">
              <input type="text" name="CA" value="<?php if($action=="edition") echo $pattern["CA"]; ?>">
            </td>
          </tr>
          <tr>
            <td width="300"><b>Vigueur :</b></td>
            <td width="300">
              <input type="text" name="vigueur" value="<?php if($action=="edition") echo $pattern["vigueur"]; ?>">
            </td>
          </tr>
          <tr>
            <td width="300"><b>Volonte :</b></td>
            <td width="300">
              <input type="text" name="volonte" value="<?php if($action=="edition") echo $pattern["volonte"]; ?>">
            </td>
          </tr>
          <tr>
            <td width="300"><b>Réflexes :</b></td>
            <td width="300">
              <input type="text" name="reflexes" value="<?php if($action=="edition") echo $pattern["reflexes"]; ?>">
            </td>
          </tr>
          <tr>
            <td width="300"><b>VD :</b></td>
            <td width="300">
              <input type="text" name="VD" value="<?php if($action=="edition") echo $pattern["VD"]; ?>">
            </td>
          </tr>
          <tr>
            <td width="300"><b>PV max :</b></td>
            <td width="300">
              <input type="text" name="pv_max" value="<?php if($action=="edition") echo $pattern["PV_max"]; ?>">
            </td>
          </tr>
          <tr>
            <td width="300"><b>Résistances :</b></td>
            <td width="300">
              <textarea rows="5" cols="40" name="resistances"><?php if($action=="edition") echo $pattern["resistance"]; ?></textarea>
            </td>
          </tr>
          <tr>
            <td width="300"><b>Description sorts :</b></td>
            <td width="300">
              <textarea rows="5" cols="40" name="sorts"><?php if($action=="edition") echo $pattern["description_sorts"]; ?></textarea>
            </td>
          </tr>
          <tr>
            <td width="300"><b>Description :</b></td>
            <td width="300">
              <textarea rows="5" cols="40" name="description"><?php if($action=="edition") echo $pattern["description"]; ?></textarea>
            </td>
          </tr>
          <tr>
          <td colspan="2">
            <input type="submit" name="submit" value="Enregistrer">
          </td>
        </tr>
        </table>
      </form>
    </div>
  </body>
</html>
