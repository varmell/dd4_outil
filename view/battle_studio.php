<html>
  <head>
    <?php include 'config.php'; ?>
    <link rel="icon" type="image/png" href="<?php echo $domaine; ?>ressources/logo.png" />
    <title>DD4-Combats</title>
  </head>
  <body>
    <div id="head">
      <?php
      include 'header.php';
      include '../modele/fonction_battle.php';
      ?>
    </div>
    <?php
      if(isset($_GET['supprimer'])){
        delete_battle($_GET['supprimer']);
      }
      include 'nav.php';
    ?>
    <div>
      <br>
      <center><h2>Combats</h2></center>
      <center>
        <table>
          <tr>
            <th>Id</th>
            <th>Nom</th>
            <th>Action</th>
            <?php $combats= select_battle(); ?>
          </tr>
          <tr>
            <?php while ($combat = $combats->fetch()) {
              ?>
              <tr>
                <td><?php echo $combat['id']; ?></td>
                <td><?php echo $combat['nom']; ?></td>
                <td>
                  <?php
                    if(empty($_SESSION["login"])){
                    echo "Vous devez être connecté pour modifier des données";
                  }
                  else { ?>
                    <a href="gestion_combat?action=edition&amp;id=<?php echo $combat['id']; ?>">Modifier</a>
                    <a href="gestion_participant?id=<?php echo $combat['id']; ?>">Ajouter des combattants</a>
                    <a href="un_combat?id=<?php echo $combat['id']; ?>">Lancer</a>
                    <a href="combat?supprimer=<?php echo $combat['id']; ?>">Supprimer</a>
                    <?php
                  }?>
                </td>
              </tr>
              <?php
            }
            ?>
          </tr>
        </table>
        <br>
        <?php if(!empty($_SESSION['login'])){
          ?>
          <form action="gestion_combat?action=nouveau" method="post">
             <input type="submit" name="Nouveau" value="Nouveau combat"/>
          </form>
          <?php
        }?>
      </center>
    </body>
    </div>

</html>
