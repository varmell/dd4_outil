<html>
  <head>
    <?php include 'config.php'; ?>
    <link rel="icon" type="image/png" href="<?php echo $domaine; ?>ressources/logo.png" />
    <title>DD4-Création de modèles de monstres</title>
  </head>
  <body>
    <div id="head">
      <?php
        include 'header.php';
        include '../modele/fonction_pattern.php';
      ?>
      <script type="text/javascript" src="ressources/js/div_masquer.js"></script>
    </div>
    <?php
      if(isset($_GET['supprimer'])){
        delete_pattern($_GET['supprimer']);
      }
      include 'nav.php';
    ?>
    <div>
      <br>
      <center><h2>Modèles de monstres</h2></center>
      <center>
        <table>
          <tr>
            <th>Id</th>
            <th>Nom</th>
            <th>PV_max</th>
            <th>CA</th>
            <th>Vigueur</th>
            <th>Volonté</th>
            <th>Réflexes</th>
            <th>Bonus initiative</th>
            <th>VD</th>
            <th>Résistance</th>
            <th>Description sort</th>
            <th>Description</th>
            <th>Action</th>
            <?php $patterns = select_pattern(); ?>
          </tr>
          <tr>
            <?php while ($pattern = $patterns->fetch()) {
              ?>
              <tr>
                <td><?php echo $pattern['id']; ?></td>
                <td><?php echo $pattern['nom']; ?></td>
                <td><p title="pv max"><?php echo $pattern['PV_max']; ?></p></td>
                <td><p title="CA"><?php echo $pattern['CA']; ?></p></td>
                <td><p title="vigueur"><?php echo $pattern['vigueur']; ?></p></td>
                <td><p title="volonté"><?php echo $pattern['volonte']; ?></p></td>
                <td><p title="réflexes"><?php echo $pattern['reflexes']; ?></p></td>
                <td><p title="bonus initiative"><?php echo $pattern['bonus_initiative']; ?></p></td>
                <td><p title="VD"><?php echo $pattern['VD']; ?></p></td>
                <td><?php echo $pattern['resistance']; ?></td>
                <td>
                  <button type="button" onclick="toggle_div(this,'<?php echo $pattern['id']; ?>');">Afficher</button>
                  <!-- Un div caché avec un attribut id -->
                  <div id="<?php echo $pattern['id']; ?>" style="display:none;">
                    <?php echo $pattern['description_sorts'];?>
                  </div>
                </td>
                <td><?php echo $pattern['description']; ?></td>
                <td>
                  <?php
                    if(empty($_SESSION["login"])){
                    echo "Vous devez être connecté pour modifier des données";
                  }
                  else { ?>
                    <a href="gestion_modele?action=edition&amp;id=<?php echo $pattern['id']; ?>">Modifier</a>
                    <a href="modele_monstre?supprimer=<?php echo $pattern['id']; ?>">Supprimer</a>
                    <?php
                  }?>
                </td>
              </tr>
              <?php
            }
            ?>
          </tr>
        </table>
        <br>
        <?php if(!empty($_SESSION['login'])){
          ?>
          <form action="gestion_modele?action=nouveau" method="post">
             <input type="submit" name="Nouveau" value="Nouveau modèle"/>
          </form>
          <?php
        }?>
      </center>
    </div>
  </body>
</html>
