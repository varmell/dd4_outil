<html>
  <head>
    <?php include 'config.php'; ?>
    <link rel="icon" type="image/png" href="<?php echo $domaine; ?>ressources/logo.png" />
    <title>DD4-Formulaire d'ajout de combattants</title>
    <meta charset="utf-8" />
  </head>
  <body>
    <div id="head">
      <?php
        include 'header.php';
        include '../modele/fonction_monster.php';
        include '../modele/fonction_participate.php';
        include '../modele/fonction_caracter.php';
        include '../modele/fonction_pattern.php';
        include 'nav.php';
      ?>
    </div>
    <div>
      <center>
        <br>
        <h2>Combattants</h2>
        <br>
          <?php $joueurs = select_caracter(); ?>
          <?php $patterns = select_pattern(); ?>
        <form action="<?php echo $domaine; ?>controller/participate_controller.php?id=<?php echo $_GET['id'] ?>" method="post">
          <table>
            <tr>
              <td>
                <label>Joueurs</label><br><br><hr>
                <?php while ($joueur = $joueurs->fetch()) {
                  ?>
                  <input type="checkbox" name="checkbox.<?php echo $joueur["id"]; ?>"> <label><?php echo $joueur["nom"]; ?></label>
                  <br>
                <?php } ?>
              </td>
              <td>
                <label>Monstres</label><br><br><hr>
                <input type="number" min="0" name="nombre_monstre" />
                <select name="monstre">
                <?php while ($pattern = $patterns->fetch()) {
                  echo "<option value=".$pattern['id']." >".$pattern['nom']."</option>";
                } ?>
                </select>
              </td>
            </tr>
          </table><br>
          <input type="submit" name="submit" value="Ajouter" />
        </form>
      </center>
    </div>
  </body>
</html>
