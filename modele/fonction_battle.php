<?php

  function insert_new_battle($post){
    //insertion d'un nouveau combat
    include 'bdd.php';
    $nom = $post["nom"];

    $sql = "INSERT INTO combats
            VALUES (null,:nom);";
    $req = $pdo->prepare($sql);

    $req->bindValue('nom', $nom, PDO::PARAM_STR);
    $req->execute();

    return $req;
  }

  function select_battle(){
    //renvoie tous les combats
    include 'bdd.php';
    $req = $pdo->prepare("SELECT *  from combats;");
    $req -> execute();
    return $req;
  }

  function delete_battle($id){
    //supprime un combat et vide la table participant en conséquence
    include 'bdd.php';
    include 'fonction_participate.php';
    include 'fonction_monster.php';
    delete_all_participant($id);
    delete_monster_fight($id);
    $req = $pdo->prepare("DELETE FROM combats WHERE id = :id;");
    $req->bindParam('id', $id, PDO::PARAM_INT);
    $req->execute();
    return $req;
  }

  function get_battle($id){
    //renvoie un combat
    include 'bdd.php';
    $req = $pdo->prepare("SELECT * FROM combats WHERE id = :id;");
    $req->bindParam('id', $id, PDO::PARAM_INT);
    $req->execute();
    return $req->fetch();
  }

  function get_battle_name($id){
    //renvoie le nom d'un combat
    include 'bdd.php';
    $req = $pdo->prepare("SELECT nom FROM combats WHERE id = :id;");
    $req->bindParam('id', $id, PDO::PARAM_INT);
    $req->execute();
    return $req->fetch();
  }

  function update_battle($post){
    include 'bdd.php';
    $nom = $post["nom"];
    $id = $post["id"];

    $sql = "UPDATE combats SET
            nom = :nom
            WHERE id = :id;";
    $req = $pdo->prepare($sql);

    $req->bindValue('nom', $nom, PDO::PARAM_STR);
    $req->bindValue('id', $id, PDO::PARAM_INT);
    return $req->execute();
  }

 ?>
