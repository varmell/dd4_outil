<?php

  function insert_new_pattern($post){
    //ajoute un modèle de monstre
    include 'bdd.php';
    $nom = $post["nom"];
    $bonus_initiative = $post["initiative"];
    $CA = $post["CA"];
    $vigueur = $post["vigueur"];
    $volonte = $post["volonte"];
    $reflexes = $post["reflexes"];
    $resistance = $post["resistances"];
    $pv_max = $post["pv_max"];
    $VD = $post["VD"];
    $description_sort = $post["sorts"];
    $description = $post["description"];

    $sql = "INSERT INTO modeles_monstres
            VALUES (null,:nom,:pv_max,:CA,:vigueur,:reflexes,:volonte,:VD,:resistance,:description_sort,:description,:bonus_initiative);";
    $req = $pdo->prepare($sql);

    //youpi ! tout un tas d'instructions pour associer les valeurs aux paramètres
    $req->bindValue('nom', $nom, PDO::PARAM_STR);
    $req->bindValue('bonus_initiative', $bonus_initiative, PDO::PARAM_INT);
    $req->bindValue('CA', $CA, PDO::PARAM_INT);
    $req->bindValue('vigueur', $vigueur, PDO::PARAM_INT);
    $req->bindValue('volonte', $volonte, PDO::PARAM_INT);
    $req->bindValue('reflexes', $reflexes, PDO::PARAM_INT);
    $req->bindValue('resistance', $resistance, PDO::PARAM_STR);
    $req->bindValue('pv_max', $pv_max, PDO::PARAM_INT);
    $req->bindValue('VD', $VD, PDO::PARAM_INT);
    $req->bindValue('description_sort', $description_sort, PDO::PARAM_STR);
    $req->bindValue('description', $description, PDO::PARAM_STR);
    $req->execute();

    return $req;
  }

  function select_pattern(){
    //renvoie tous les modèles de monstres
    include 'bdd.php';
    $req = $pdo->prepare("SELECT *  from modeles_monstres ORDER BY nom ASC;");
    $req -> execute();
    return $req;
  }

  function delete_pattern($id){
    //supprime un modèle de monstres
    include 'bdd.php';
    $req = $pdo->prepare("DELETE FROM modeles_monstres WHERE id = :id;");
    $req->bindParam('id', $id, PDO::PARAM_INT);
    $req->execute();
    return $req;
  }

  function get_pattern($id){
    //renvoie un modèles de monstres
    include 'bdd.php';
    $req = $pdo->prepare("SELECT * FROM modeles_monstres WHERE id = :id;");
    $req->bindParam('id', $id, PDO::PARAM_INT);
    $req->execute();
    return $req->fetch();
  }

  function update_pattern($post){
    //met à jour un modèles de monstres
    include 'bdd.php';
    $nom = $post["nom"];
    $bonus_initiative = $post["initiative"];
    $CA = $post["CA"];
    $vigueur = $post["vigueur"];
    $volonte = $post["volonte"];
    $reflexes = $post["reflexes"];
    $resistance = $post["resistances"];
    $pv_max = $post["pv_max"];
    $VD = $post["VD"];
    $description_sort = $post["sorts"];
    $description = $post["description"];
    $id = $post["id"];

    $sql = "UPDATE modeles_monstres SET
            nom = :nom,
            bonus_initiative = :bonus_initiative,
            CA = :CA,
            vigueur = :vigueur,
            volonte = :volonte,
            reflexes = :reflexes,
            resistance = :resistance,
            Pv_max = :pv_max,
            VD = :VD,
            description_sorts = :description_sort,
            description = :description
            WHERE id = :id;";
    $req = $pdo->prepare($sql);

    //youpi ! tout un tas d'instructions pour associer les valeurs aux paramètres et oui encore ce commentaire
    $req->bindValue('nom', $nom, PDO::PARAM_STR);
    $req->bindValue('bonus_initiative', $bonus_initiative, PDO::PARAM_INT);
    $req->bindValue('CA', $CA, PDO::PARAM_INT);
    $req->bindValue('vigueur', $vigueur, PDO::PARAM_INT);
    $req->bindValue('volonte', $volonte, PDO::PARAM_INT);
    $req->bindValue('reflexes', $reflexes, PDO::PARAM_INT);
    $req->bindValue('resistance', $resistance, PDO::PARAM_STR);
    $req->bindValue('pv_max', $pv_max, PDO::PARAM_INT);
    $req->bindValue('VD', $VD, PDO::PARAM_INT);
    $req->bindValue('description_sort', $description_sort, PDO::PARAM_STR);
    $req->bindValue('description', $description, PDO::PARAM_STR);
    $req->bindValue('id', $id, PDO::PARAM_INT);
    return $req->execute();
  }

 ?>
