<?php
  include '../modele/fonction_pattern.php';
  include 'verif_connecter_controller.php';

  if(empty($_POST["nom"]) || empty($_POST["initiative"]) || empty($_POST["VD"]) || empty($_POST["CA"]) || empty($_POST["vigueur"]) || empty($_POST["reflexes"])
  || empty($_POST["resistances"]) || empty($_POST["pv_max"]) || empty($_POST["sorts"]) || empty($_POST["description"]) || empty($_POST["volonte"])){

    //un des champs n'est pas remplis
    echo "Un des champs est vide ou bonus_initiative est à 0. Tout doit être renseigné";
    if($_GET["action"]=='edition'){
      header("Refresh: 3;URL=../gestion_modele?action=edition&id=".$_POST['id']);
    }else
    header("Refresh: 3;URL= ../gestion_modele?action=nouveau");
  }
  else {
    //Super tout les champs sont remplis
    if(strpbrk($_POST["nom"], '<?') || strpbrk($_POST["initiative"], '<?') || strpbrk($_POST["VD"], '<?') || strpbrk($_POST["CA"], '<?'
  || strpbrk($_POST["vigueur"], '<?') || strpbrk($_POST["reflexes"], '<?') || strpbrk($_POST["volonte"], '<?') || strpbrk($_POST["resistances"], '<?') || strpbrk($_POST["pv_max"], '<?')
|| strpbrk($_POST["sorts"], '<?')) || strpbrk($_POST["description"], '<?')){
      echo "Une balise php a été détecté veuilliez ne pas en mettre.";
      $redir = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '../accueil';
      header("Refresh: 3;URL=".$redir);
      exit();
    }
    if($_GET["action"]=="nouveau"){
      $verif = insert_new_pattern($_POST);
      if($verif){
        header("Location: ../modele_monstre");
      }
      else{
        echo "Une erreur est survenue lors de la création du modèle";
        header("Refresh: 3;URL= ../gestion_modele?action=nouveau");
      }
    }
    elseif ($_GET["action"]=="edition") {
      $verif = update_pattern($_POST);
      if($verif){
        header("Location: ../modele_monstre");
      }
      else {
        echo "Une erreur est survenue lors de la mise à jour du modèle";
        header("Refresh: 3;URL=../gestion_modele?action=edition");
      }
    }
  }
?>
