<html>
  <head>
    <?php include 'config.php'; ?>
    <link rel="icon" type="image/png" href="<?php echo $domaine; ?>ressources/logo.png" />
    <title>DD4-Personnages</title>
  </head>
  <body>
    <div id="head">
      <?php
      include 'header.php';
      include '../modele/fonction_caracter.php';
      ?>
    </div>
    <?php
      if(isset($_GET['supprimer'])){
        delete_caracter($_GET['supprimer']);
      }
      include 'nav.php';
    ?>
    <div>
      <br>
      <center><h2>Personnage</h2></center>
      <center>
        <table>
          <tr>
            <th>Id</th>
            <th>Nom</th>
            <th>PV_actuel</th>
            <th>PV_max</th>
            <th>Bonus d'initiative</th>
            <th>Initiative</th>
            <th>CA</th>
            <th>Vigueur</th>
            <th>Volonté</th>
            <th>Réflexes</th>
            <th>Action</th>
            <?php $joueurs = select_caracter(); ?>
          </tr>
          <tr>
            <?php while ($joueur = $joueurs->fetch()) {
              ?>
              <tr>
                <td><?php echo $joueur['id']; ?></td>
                <td><?php echo $joueur['nom']; ?></td>
                <td><?php echo $joueur['PV_actuel']; ?></td>
                <td><?php echo $joueur['PV_max']; ?></td>
                <td><?php echo $joueur['bonus_initiative']; ?></td>
                <td><?php echo $joueur['initiative']; ?></td>
                <td><?php echo $joueur['CA']; ?></td>
                <td><?php echo $joueur['vigueur']; ?></td>
                <td><?php echo $joueur['volonte']; ?></td>
                <td><?php echo $joueur['reflexes']; ?></td>
                <td>
                  <?php
                    if(empty($_SESSION["login"])){
                    echo "Vous devez être connecté pour modifier des données";
                  }
                  else { ?>
                    <a href="gestion_personnage?action=edition&amp;id=<?php echo $joueur['id']; ?>">Modifier</a>
                    <a href="personnage?supprimer=<?php echo $joueur['id']; ?>">Supprimer</a>
                    <?php
                  }?>
                </td>
              </tr>
              <?php
            }
            ?>
          </tr>
        </table>
        <br>
        <?php if(!empty($_SESSION['login'])){
          ?>
          <form action="gestion_personnage?action=nouveau" method="post">
             <input type="submit" name="Nouveau" value="Nouveau personnage"/>
          </form>
          <?php
        }?>
      </center>
    </div>
  </body>
</html>
