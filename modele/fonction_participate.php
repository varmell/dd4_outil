<?php

  function insert_new_participant($post){
    //ajoute un participant
    include 'bdd.php';
    $id_joueurs = $post["id_joueurs"];
    $id_combats = $post["id_combats"];

    $sql = "INSERT INTO participe_combat
            VALUES (:id_joueurs,:id_combats);";
    $req = $pdo->prepare($sql);

    $req->bindValue('id_joueurs', $id_joueurs, PDO::PARAM_INT);
    $req->bindValue('id_combats', $id_combats, PDO::PARAM_INT);
    try {
          $req->execute();
    } catch (PDOException $e) {
      if($e->errorInfo[1] == 1062){
        header("Location: combat");
      }
    }
    return $req;
  }

  function select_participant(){
    //renvoie tous les participants
    include 'bdd.php';
    $req = $pdo->prepare("SELECT *  from participe_combat;");
    $req -> execute();
    return $req;
  }

  function delete_participant_caracter($id_joueurs){
    //supprime toute les participations d'un joueur
    include 'bdd.php';
    $req = $pdo->prepare("DELETE FROM participe_combat WHERE id_joueurs = :id_joueurs;");
    $req->bindParam('id_joueurs', $id_joueurs, PDO::PARAM_INT);
    $req->execute();
    return $req;
  }

  function delete_participant($id_joueurs, $id_combats){
    //supprime la participation d'un  joueur à un combat
    include 'bdd.php';
    $req = $pdo->prepare("DELETE FROM participe_combat WHERE id_joueurs = :id_joueurs and id_combats = :id_combats;");
    $req->bindParam('id_joueurs', $id_joueurs, PDO::PARAM_INT);
    $req->bindParam('id_combats', $id_combats, PDO::PARAM_INT);
    $req->execute();
    return $req;
  }

  function delete_all_participant($id_combats){
    //supprime tous les participants à un combat
    include 'bdd.php';
    $req = $pdo->prepare("DELETE FROM participe_combat WHERE id_combats = :id_combats;");
    $req->bindParam('id_combats', $id_combats, PDO::PARAM_INT);
    $req->execute();
    return $req;
  }

  function get_participant($id_joueurs, $id_combats){
    //renvoie un participant
    include 'bdd.php';
    $req = $pdo->prepare("SELECT * FROM participe_combat WHERE id_joueurs = :id_joueurs and id_combats = :id_combats;");
    $req->bindParam('id_joueurs', $id_joueurs, PDO::PARAM_INT);
    $req->bindParam('id_combats', $id_combats, PDO::PARAM_INT);
    $req->execute();
    return $req->fetch();
  }

  function get_all_participant($id_combats){
    //renvoie tous les participants d'un combat
    include 'bdd.php';
    $req = $pdo->prepare("SELECT * FROM participe_combat WHERE id_combats = :id_combats;");
    $req->bindParam('id_combats', $id_combats, PDO::PARAM_INT);
    $req->execute();
    return $req;
  }

 ?>
