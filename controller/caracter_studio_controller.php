<?php
  include '../modele/fonction_caracter.php';
  include 'verif_connecter_controller.php';

  if(empty($_POST["nom"]) || empty($_POST["initiative"]) || empty($_POST["bonus_initiative"]) || empty($_POST["CA"]) || empty($_POST["vigueur"]) || empty($_POST["reflexes"])
  || empty($_POST["pv_actuel"]) || empty($_POST["pv_max"]) || empty($_POST["volonte"])){

    //un des champs n'est pas remplis
    echo "Un des champs est vide. Tout doit être renseigné";
    header("Refresh: 3;URL=../gestion_personnage?action=".$_GET['action']);
  }
  else {
    //Super tout les champs sont remplis
    if(strpbrk($_POST["nom"], '<?') || strpbrk($_POST["initiative"], '<?') || strpbrk($_POST["bonus_initiative"], '<?') || strpbrk($_POST["CA"], '<?'
  || strpbrk($_POST["vigueur"], '<?') || strpbrk($_POST["reflexes"], '<?') || strpbrk($_POST["volonte"], '<?') || strpbrk($_POST["pv_actuel"], '<?') || strpbrk($_POST["pv_max"], '<?'))){
      echo "Une balise php a été détecté veuilliez ne pas en mettre.";
      $redir = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '../accueil';
      header("Refresh: 3;URL=".$redir);
      exit();
    }
    if($_GET["action"]=="nouveau"){
      $verif = insert_new_caracter($_POST);
      if($verif){
        header("Location: ../personnage");
      }
      else{
        echo "Une erreur est survenue lors de la création du personnage";
        header("Refresh: 3;URL=../gestion_personnage");
      }
    }
    elseif ($_GET["action"]=="edition") {
      $verif = update_caracter($_POST);
      if($verif){
        header("Location: ../personnage");
      }
      else {
        echo "Une erreur est survenue lors de la mise à jour du personnage";
        header("Refresh: 3;URL=../gestion_personnage");
      }
    }
  }
?>
