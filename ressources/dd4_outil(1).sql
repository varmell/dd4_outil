-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Sam 03 Décembre 2016 à 13:51
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `dd4_outil`
--

-- --------------------------------------------------------

--
-- Structure de la table `combats`
--

CREATE TABLE IF NOT EXISTS `combats` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Contenu de la table `combats`
--

INSERT INTO `combats` (`id`, `nom`) VALUES
(8, 'test'),
(13, 'fgdgfd');

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `infos_joueurs_combat`
--
CREATE TABLE IF NOT EXISTS `infos_joueurs_combat` (
`id_joueur` int(11)
,`nom_joueur` varchar(32)
,`PV_actuel` int(3)
,`PV_max` int(3)
,`CA` int(2)
,`initiative` int(2)
,`reflexes` int(2)
,`vigueur` int(2)
,`volonte` int(2)
,`id_combat` int(11)
,`nom` varchar(32)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `infos_monstres_combat`
--
CREATE TABLE IF NOT EXISTS `infos_monstres_combat` (
`nom_modele` varchar(32)
,`PV_actuel` int(3)
,`PV_max` int(3)
,`CA` int(2)
,`vigueur` int(2)
,`reflexes` int(2)
,`volonte` int(2)
,`VD` int(1)
,`resistance` text
,`description_sorts` text
,`description` text
,`bonus_initiative` int(2)
,`divers` text
,`initiative` int(2)
,`id` int(11)
,`id_combats` int(11)
,`id_modeles` int(11)
);
-- --------------------------------------------------------

--
-- Structure de la table `joueurs`
--

CREATE TABLE IF NOT EXISTS `joueurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(32) NOT NULL,
  `bonus_initiative` int(2) NOT NULL,
  `CA` int(2) NOT NULL,
  `vigueur` int(2) NOT NULL,
  `volonte` int(2) NOT NULL,
  `reflexes` int(2) NOT NULL,
  `PV_actuel` int(3) NOT NULL,
  `PV_max` int(3) NOT NULL,
  `initiative` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `joueurs`
--

INSERT INTO `joueurs` (`id`, `nom`, `bonus_initiative`, `CA`, `vigueur`, `volonte`, `reflexes`, `PV_actuel`, `PV_max`, `initiative`) VALUES
(3, 'Erik le rouge', 2, 15, 13, 16, 14, 1, 37, 25),
(4, 'Torinn', 2, 20, 15, 12, 11, 3, 35, 20),
(5, 'Wilhelm', 4, 17, 14, 11, 17, 10, 28, 24),
(6, 'Chaedi', 5, 17, 15, 16, 17, 17, 28, 5);

-- --------------------------------------------------------

--
-- Structure de la table `modeles_monstres`
--

CREATE TABLE IF NOT EXISTS `modeles_monstres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(32) NOT NULL,
  `PV_max` int(3) NOT NULL,
  `CA` int(2) NOT NULL,
  `vigueur` int(2) NOT NULL,
  `reflexes` int(2) NOT NULL,
  `volonte` int(2) NOT NULL,
  `VD` int(1) NOT NULL,
  `resistance` text NOT NULL,
  `description_sorts` text NOT NULL,
  `description` text NOT NULL,
  `bonus_initiative` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `modeles_monstres`
--

INSERT INTO `modeles_monstres` (`id`, `nom`, `PV_max`, `CA`, `vigueur`, `reflexes`, `volonte`, `VD`, `resistance`, `description_sorts`, `description`, `bonus_initiative`) VALUES
(2, 'sbire kobold', 1, 15, 11, 13, 11, 6, 'Aucune\r\n\r\n\r\n', '<b>Lance(simple; volontÃ©) :</b> <br>\r\n+4 contre CA ; 4 dÃ©gÃ¢ts<br>\r\n<b>Javeline(simple;volontÃ©) :</b><br>\r\nDistance 10/20;+4 contre CA; 4 dÃ©gÃ¢ts<br>\r\n<b>Fuyant(mineure;volontÃ©)</b><br>\r\nLe sbire kobold peut se dÃ©caler de 1 case au prix d''une action mineure.\r\n', '-2 a toutes les dÃ©fenses quand il n''est pas adjacent Ã  un autre sbire kobold.<br>\r\nCes petits humanoÃ¯des reptiliens ont des Ã©cailles rouge foncÃ©. Il porte une armure de peau, une lance, 3 javelines et un bouclier lÃ©ger.', 3),
(3, 'draco-Ã©cu kobold', 36, 18, 14, 13, 13, 6, 'Aucune', '<b>EpÃ©e courte(simple;volontÃ©)</b> <br>\r\n+7 contre CA;1d6 +3 dÃ©gÃ¢ts, et la cible est marquÃ©e jusqu''Ã  la fin du tour de jeu prochain du draco-Ã©cu kobold.<br> \r\n<b>Tactique du draco-Ã©cu</b> (rÃ©action immÃ©diate, quand un ennemi adjacent s''Ã©loigne en se dÃ©calant ou quand un ennemi se dÃ©place de maniÃ¨re Ã  lui Ãªtre adjacent; volontÃ©)<br> \r\nLe draco-Ã©cu peut se dÃ©caler de 1 case. <br>\r\n<b>Attaque en nombre</b> <br>\r\nLe draco-Ã©cu kobold bÃ©nÃ©ficie d''un bonus de +1 aux jets d''attaque par alliÃ© kobold adjacent Ã  la cible. <br>\r\n<b>Fuyant(mineure; volontÃ©)</b> <br>\r\nLe draco-Ã©cu kobold peut se dÃ©caler de 1 case au prix d''une action mineure.', 'HumanoÃ¯de reptiliens aux Ã©cailles rouges porte une Ã©pÃ©e courte et ce qui ressemble Ã  une Ã©caille de dragon en guise de bouclier.', 4),
(4, 'frondeur kobold', 24, 13, 12, 14, 12, 6, 'Aucune', '<b>Dague </b>(simple;volontÃ©)<br>\r\n+5 contre CA; 1d4 + 3 dÃ©gÃ¢ts <br>\r\n<b>Fronde</b>(simple;volontÃ©)<br>\r\nDistance 10/20;+6 contre CA; 1d6 +3 dÃ©gÃ¢ts <br>\r\n<b> Tir spÃ©cial </b> <br>\r\nLe frondeur kobold peut utiliser des munitions spÃ©ciales avec sa fronde. Il dispose gÃ©nÃ©ralement de trois billes pour tir spÃ©cial, comme la liste ci-dessous. Une attaque de <i>tir spÃ©cial </i> qui inflige des dÃ©gÃ¢ts entraÃ®ne Ã©galement un effet supplÃ©mentaire qui dÃ©pend de son type : <br>\r\n2 billes ardentes (feu) : la cible subit 2 dÃ©gÃ¢ts de feu continus (sauvegarde annule). <br>\r\n1 bille gluante : la cible est immobilisÃ©e (sauvegarde annule). <br>\r\n<b> Fuyant </b>(mineure; volontÃ©)<br>\r\nLe frondeur kobold peut se dÃ©caler de 1 case au prix d''une action mineure."); ', 'Cette petite crÃ©ature reptilienne a des Ã©cailles de couleur rouille. Elle porte une armure cuir et une fronde avec 20 billes. Trois minuscules globes en cÃ©ramique pendent Ã  la bandouliÃ¨re qu''elle porte en travers de la poitrine. ', 3),
(5, 'franc-tireur kobold', 27, 15, 11, 14, 13, 6, 'Aucune', '<b>Lance</b>(simple;volontÃ©)<br>\r\n+6 contre CA; 1d8 dÃ©gÃ¢ts<br>\r\n<b>Avantage de combat </b><br>\r\nLes attaques de corps Ã  corps et Ã  distance du franc-tireur kobold infligent 1d6 dÃ©gÃ¢ts supplÃ©mentaires Ã  toute cible contre laquelle il jouit d''un avantage de combat.<br>\r\n<b>Attaque en nombre</b><br>\r\nLe franc-tireur kobold bÃ©nÃ©ficie d''un bonus de +1 aux jets d''attaque par alliÃ© kobold adjacent Ã  la cible.<br>\r\n<b>Fuyant</b>(mineure; volontÃ©)<br>\r\nLe franc-tireur kobold peut se dÃ©caler de 1 case au prix d''une action mineure.', 'Cette agile crÃ©ature reptilienne est recouverte d''Ã©cailles rouge et brun, porte une armure de cuir sombre, et manie une lance et un bouclier lÃ©ger.', 5),
(6, 'dracoprÃªtre kobold', 36, 17, 13, 15, 15, 6, 'Aucune', '<b>Lance</b>(simple;volontÃ©)<br>\r\n+7 contre CA; 1d8 dÃ©gÃ¢ts<br>\r\n<b>Orbe d''Ã©nergie</b>(simple;volontÃ©)acide<br>\r\nDistance 10; +6 contre RÃ©flexes; 1d10 +3 dÃ©gÃ¢ts d''acide.<br>\r\n<b>Foi insufflÃ©</b>(mineure; rencontre)<br>\r\nExplosion de proximitÃ© de 10; les alliÃ©s kobolds pris dans l''explosion gagnent 5 points de vie temporaires et peuvent se dÃ©caler de 1 case.<br>\r\n<b>Souffle de dragon</b>(simple;rencontre) acide <br>\r\nDÃ©charge de proximitÃ© 3; +6 contre Vigueur; 1d10 + 3dÃ©gÃ¢ts d''acide. <i>Echec</i> : demi-dÃ©gÃ¢ts.<br>\r\n<b>Fuyant</b>(mineure;volontÃ©)<br>\r\nLe dracoprÃªtre kobold peut se dÃ©caler de 1 case au prix d''une action mineure.<br>', 'Cet humanoÃ¯de reptilien porte un masque en os taillÃ© Ã  l''image d''une tÃªte de dragon. Il brandit Ã©galement une lance et porte une armure de peau rouge vif.', 4),
(7, 'nuÃ©e de rats', 36, 15, 12, 14, 11, 4, 'demi-dÃ©gÃ¢ts contre les attaques de corps Ã  corps et Ã  distance ; VulnÃ©rabilitÃ© subit +5 dÃ©gÃ¢ts contre les attaques de proximitÃ© et de zone.', '<b>Morsure grouillante</b>(simple;volontÃ©)<br>\r\n+6 contre CA; 1d6 +3 dÃ©gÃ¢ts, et 3 dÃ©gÃ¢ts continus (sauvegarde annule).<br>\r\n<b>Attaque nuÃ©e</b> aura 1; la nuÃ©e de rats effectue une attaque de base au prix d''une action libre contre chaque ennemi qui dÃ©bute son tour de jeu dans l''aura.', 'Des dizaines de rats couinent et mordent tout ce qui leur passe sous la dent, se grimpant les uns sur les autres telle une masse grouillante de fourrure, de dents et de griffes.', 6),
(8, 'archer elfe', 32, 15, 11, 13, 12, 7, 'aucune', '<b>EpÃ©e courte</b>(simple;volontÃ©)<br>\r\n+5 contre CA; 1d6 +4 dÃ©gÃ¢ts<br>\r\n<b>Arc long</b>(simple;volontÃ©)<br>\r\nDistance 20/40; +7 contre CA; 1d10 +4 dÃ©gÃ¢ts<br>\r\n<b>MobilitÃ© de l''archer</b><br>\r\nSi l''archer elfe se dÃ©place d''au moins 4 cases depuis sa position de dÃ©part, il bÃ©nÃ©ficie d''un bonus de +2 aux jets d''attaque Ã  distance jusqu''au dÃ©but de son tour de jeu suivant.<br>\r\n<b>PrÃ©cision elfique</b>(libre;rencontre)<br>\r\nL''elfe peut relancer un jet d''attaque. Il doit accepter le rÃ©sultat du second jet, mÃªme s''il est infÃ©rieur.<br>\r\n<b>Pas si prÃ¨s</b>(rÃ©action immÃ©diate, quand un ennemi effectue une attaque de corps Ã  corps contre l''archer elfe; rencontre)<br>\r\nL''archer elfe se dÃ©cale de 1 case et effectue une attaque Ã  distance contre l''ennemi.<br>\r\n<b>Pas assurÃ©</b><br>\r\nL''elfe ignore le terrain difficile lorsqu''il se dÃ©cale.', 'Armure de cuir, Ã©pÃ©e courte, arc long, carquois de 30 flÃ¨ches', 5),
(9, 'tireur d''Ã©lite gobelin', 31, 16, 14, 16, 13, 6, 'aucune', '<b>EpÃ©e courte</b>(simple;volontÃ©)<br>\r\n+6 contre CA;1d6 +2 dÃ©gÃ¢ts.<br>\r\n<b>ArbalÃ¨te</b>(simple;volontÃ©)<br>\r\nDistance 15/30; +9 contre CA; 1d8 +4 dÃ©gÃ¢ts.<br>\r\n<b>Sniper</b><br>\r\nQuand un tireur d''Ã©lite gobelin effectue une attaque Ã  distance depuis une cachette et rate, on considÃ¨re qu''il est toujours cachÃ©.<br>\r\n<b>Avantage de combat</b><br>\r\nLe tireur d''Ã©lite gobelin inflige 1d6 dÃ©gÃ¢ts supplÃ©mentaire Ã  toute cible contre laquelle il jouit d''un avantage de combat.<br>\r\n<b>Tactique gobeline</b>(rÃ©action immÃ©diate, quand le gobelin est ratÃ© par une attaque de corps Ã  corps; volontÃ©)<br>\r\nLe tireur d''Ã©lite gobelin peut se dÃ©caler de 1 case quand une attaque de corps Ã  corps le rate.<br>', 'EpÃ©e courte, armure de cuir, arbalÃ¨te et 20 carreaux.', 5),
(10, 'homme d''armes gobelin', 29, 17, 13, 15, 12, 6, 'aucune', '<b>Lance</b>(simple;volontÃ©)<br>\r\n+6 centre CA; 1d8 +2 dÃ©gÃ¢ts<br>\r\n<b>Javeline</b>(simple;volontÃ©)<br>\r\nDistance 10/20; +6 contre CA; 1d6 +2 dÃ©gÃ¢ts<br>\r\n<b>Bonne position</b><br>\r\nSi, Ã  son tour de jeu, l''homme d''armes gobelin termine son dÃ©placement Ã  au moins 4 cases de son point de dÃ©part, ses attaques Ã  distance infligent 1d6 dÃ©gÃ¢ts supplÃ©mentaires jusqu''au dÃ©but de son tour de jeu suivant. <br>\r\n<b>Attaque Ã  distance mobile</b><br>\r\nL''homme d''armes gobelin se dÃ©place jusqu''Ã  la moitiÃ© de sa VD. A n''importe quel moment de son dÃ©placement, il peut effectuer une attaque Ã  distance qui ne provoque pas d''attaque d''opportunitÃ©.<br>\r\n<b>Tactique gobeline</b>(rÃ©action immÃ©diate, quand le gobelin est ratÃ© par une attaque de corps Ã  corps; volontÃ©)<br>\r\nLe tireur d''Ã©lite gobelin peut se dÃ©caler de 1 case quand une attaque de corps Ã  corps le rate.\r\n ', 'Lance, 5 javelines, armure de cuir', 5);

-- --------------------------------------------------------

--
-- Structure de la table `monstres`
--

CREATE TABLE IF NOT EXISTS `monstres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_combats` int(11) NOT NULL,
  `id_modeles` int(11) NOT NULL,
  `PV_actuel` int(3) NOT NULL,
  `divers` text NOT NULL,
  `initiative` int(2) NOT NULL,
  PRIMARY KEY (`id`,`id_combats`,`id_modeles`),
  KEY `FK2_monstres` (`id_modeles`),
  KEY `FK1_monstres` (`id_combats`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=75 ;

--
-- Contenu de la table `monstres`
--

INSERT INTO `monstres` (`id`, `id_combats`, `id_modeles`, `PV_actuel`, `divers`, `initiative`) VALUES
(71, 8, 8, 32, '', 11),
(72, 8, 8, 32, '', 21),
(73, 8, 3, 36, '', 12),
(74, 8, 3, 36, '', 11);

-- --------------------------------------------------------

--
-- Structure de la table `participe_combat`
--

CREATE TABLE IF NOT EXISTS `participe_combat` (
  `id_joueurs` int(11) NOT NULL,
  `id_combats` int(11) NOT NULL,
  PRIMARY KEY (`id_joueurs`,`id_combats`),
  KEY `FK_2` (`id_combats`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `participe_combat`
--

INSERT INTO `participe_combat` (`id_joueurs`, `id_combats`) VALUES
(3, 8),
(4, 8),
(5, 8),
(6, 8);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE IF NOT EXISTS `utilisateurs` (
  `login` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`login`, `password`) VALUES
('dylan', 'dylan');

-- --------------------------------------------------------

--
-- Structure de la vue `infos_joueurs_combat`
--
DROP TABLE IF EXISTS `infos_joueurs_combat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `infos_joueurs_combat` AS select `j`.`id` AS `id_joueur`,`j`.`nom` AS `nom_joueur`,`j`.`PV_actuel` AS `PV_actuel`,`j`.`PV_max` AS `PV_max`,`j`.`CA` AS `CA`,`j`.`initiative` AS `initiative`,`j`.`reflexes` AS `reflexes`,`j`.`vigueur` AS `vigueur`,`j`.`volonte` AS `volonte`,`c`.`id` AS `id_combat`,`c`.`nom` AS `nom` from ((`joueurs` `j` join `participe_combat` `p`) join `combats` `c`) where ((`j`.`id` = `p`.`id_joueurs`) and (`c`.`id` = `p`.`id_combats`));

-- --------------------------------------------------------

--
-- Structure de la vue `infos_monstres_combat`
--
DROP TABLE IF EXISTS `infos_monstres_combat`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `infos_monstres_combat` AS select `mm`.`nom` AS `nom_modele`,`m`.`PV_actuel` AS `PV_actuel`,`mm`.`PV_max` AS `PV_max`,`mm`.`CA` AS `CA`,`mm`.`vigueur` AS `vigueur`,`mm`.`reflexes` AS `reflexes`,`mm`.`volonte` AS `volonte`,`mm`.`VD` AS `VD`,`mm`.`resistance` AS `resistance`,`mm`.`description_sorts` AS `description_sorts`,`mm`.`description` AS `description`,`mm`.`bonus_initiative` AS `bonus_initiative`,`m`.`divers` AS `divers`,`m`.`initiative` AS `initiative`,`m`.`id` AS `id`,`m`.`id_combats` AS `id_combats`,`m`.`id_modeles` AS `id_modeles` from ((`monstres` `m` join `modeles_monstres` `mm`) join `combats` `c`) where ((`m`.`id_combats` = `c`.`id`) and (`m`.`id_modeles` = `mm`.`id`));

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `monstres`
--
ALTER TABLE `monstres`
  ADD CONSTRAINT `FK1_monstres` FOREIGN KEY (`id_combats`) REFERENCES `combats` (`id`),
  ADD CONSTRAINT `FK2_monstres` FOREIGN KEY (`id_modeles`) REFERENCES `modeles_monstres` (`id`);

--
-- Contraintes pour la table `participe_combat`
--
ALTER TABLE `participe_combat`
  ADD CONSTRAINT `FK_2` FOREIGN KEY (`id_combats`) REFERENCES `combats` (`id`),
  ADD CONSTRAINT `ghjh` FOREIGN KEY (`id_joueurs`) REFERENCES `joueurs` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
