<?php
 function get_infos_joueur_battle($id_combat){
   //renvoie toute les infos joueurs d'un combat
  include 'bdd.php';
  $req = $pdo->prepare("SELECT * FROM infos_joueurs_combat WHERE id_combat = :id_combat ORDER BY initiative DESC;");
  $req->bindParam('id_combat', $id_combat, PDO::PARAM_INT);
  $req->execute();
  return $req;
 }
 function get_infos_monstre_battle($id_combat){
   //renvoie toute les infos monstres d'un combat
  include 'bdd.php';
  $req = $pdo->prepare("SELECT * FROM infos_monstres_combat WHERE id_combats = :id_combat ORDER BY initiative DESC;");
  $req->bindParam('id_combat', $id_combat, PDO::PARAM_INT);
  $req->execute();
  return $req;
 }
 function first_infos_monstre_battle($id_combat){
   //renvoie les informations du 1er monstre d'un combat
  include 'bdd.php';
  $req = $pdo->prepare("SELECT * FROM infos_monstres_combat WHERE id_combats = :id_combat;");
  $req->bindParam('id_combat', $id_combat, PDO::PARAM_INT);
  $req->execute();
  return $req->fetch();
}


  function order($participant){
    //renvoie  un tableau classé par ordre décroissant d'initiative
    $taille = count($participant);
    for ($i=0; $i < $taille; $i++) {
      $max = $participant[$i]['initiative'];
      $posMax = $i;
      for($j=$i; $j < $taille; $j++){
        if($participant[$j]['initiative'] > $max){
          $max = $participant[$j]['initiative'];
          $posMax = $j;
        }
      }
      $tmp = $participant[$posMax];
      $participant[$posMax] = $participant[$i];
      $participant[$i] = $tmp;
    }
    return $participant;
  }

 ?>
