<html>
  <head>
    <?php include 'config.php'; ?>
    <link rel="icon" type="image/png" href="<?php echo $domaine; ?>ressources/logo.png" />
    <title>DD4-Formulaire d'un personnage</title>
  </head>
  <body>
    <div id="head">
      <?php
      include 'header.php';
      include '../modele/fonction_caracter.php';
      include 'nav.php';
      ?>
    </div>
    <?php
      $action = $_GET["action"];
      if($action=="edition")
        $joueur = get_caracter($_GET["id"]);
    ?>
    <div>
      <center><h2>Personnage</h2></center>
      <form action="controller/caracter_studio_controller.php?action=<?php echo $action; ?>" method="post">
        <table border="0" width="400" align="center">
          <tr>
            <?php if($action=="edition") {?><td width="300"><b>Id</b></td><?php } ?>
            <td width="300">
              <input type="<?php if($action=="nouveau") echo "hidden"; else echo "text"; ?>" name="id" value="<?php if($action=="edition") echo $joueur["id"]; ?>">
            </td>
          </tr>
          <tr>
            <td width="300"><b>Nom :</b></td>
            <td width="300">
              <input type="text" name="nom" value="<?php if($action=="edition") echo $joueur["nom"]; ?>">
            </td>
          </tr>
          <tr>
            <td width="300"><b>Bonus d'initiative :</b></td>
            <td width="300">
              <input type="text" name="bonus_initiative" value="<?php if($action=="edition") echo $joueur["bonus_initiative"]; ?>">
            </td>
          </tr>
          <tr>
            <td width="300"><b>Initiative :</b></td>
            <td width="300">
              <input type="text" name="initiative" value="<?php if($action=="edition") echo $joueur["initiative"]; ?>">
            </td>
          </tr>
          <tr>
            <td width="300"><b>CA :</b></td>
            <td width="300">
              <input type="text" name="CA" value="<?php if($action=="edition") echo $joueur["CA"]; ?>">
            </td>
          </tr>
          <tr>
            <td width="300"><b>Vigueur :</b></td>
            <td width="300">
              <input type="text" name="vigueur" value="<?php if($action=="edition") echo $joueur["vigueur"]; ?>">
            </td>
          </tr>
          <tr>
            <td width="300"><b>Volonte :</b></td>
            <td width="300">
              <input type="text" name="volonte" value="<?php if($action=="edition") echo $joueur["volonte"]; ?>">
            </td>
          </tr>
          <tr>
            <td width="300"><b>Réflexes :</b></td>
            <td width="300">
              <input type="text" name="reflexes" value="<?php if($action=="edition") echo $joueur["reflexes"]; ?>">
            </td>
          </tr>
          <tr>
            <td width="300"><b>PV actuel :</b></td>
            <td width="300">
              <input type="text" name="pv_actuel" value="<?php if($action=="edition") echo $joueur["PV_actuel"]; ?>">
            </td>
          </tr>
          <tr>
            <td width="300"><b>PV max :</b></td>
            <td width="300">
              <input type="text" name="pv_max" value="<?php if($action=="edition") echo $joueur["PV_max"]; ?>">
            </td>
          </tr>
          <tr>
          <td colspan="2">
            <input type="submit" name="submit" value="Enregistrer">
          </td>
        </tr>
        </table>
      </form>
    </div>
  </body>
</html>
