<?php
  include '../modele/fonction_battle.php';
  include 'verif_connecter_controller.php';

  if(empty($_POST["nom"])){

    //un des champs n'est pas remplis
    echo "Un des champs est vide. Tout doit être renseigné";
    header("Refresh: 3;URL=../gestion_combat?action=".$_GET['action']);
  }
  else {
    //Super tout les champs sont remplis
    if(strpbrk($_POST["nom"], '<?')){
      echo "Une balise php a été détecté veuilliez ne pas en mettre.";
      $redir = !empty($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '../accueil';
      header("Refresh: 3;URL=".$redir);
      exit();
    }
    if($_GET["action"]=="nouveau"){
      $verif = insert_new_battle($_POST);
      if($verif){
        header("Location: ../combat");
      }
      else{
        echo "Une erreur est survenue lors de la création du combat";
        header("Refresh: 3;URL=../gestion_combat?action=".$_GET['action']);
      }
    }
    elseif ($_GET["action"]=="edition") {
      $verif = update_battle($_POST);
      if($verif){
        header("Location: ../combat");
      }
      else {
        echo "Une erreur est survenue lors de la mise à jour du combat";
        header("Refresh: 3;URL=../gestion_combat?action=".$_GET['action']);
      }
    }
  }
?>
