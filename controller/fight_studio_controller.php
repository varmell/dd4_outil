<?php
  include '../modele/fonction_caracter.php';
  include '../modele/bdd.php';
  include '../modele/fonction_participate.php';
  include '../modele/fonction_monster.php';
  include '../modele/fonction_pattern.php';
  include 'verif_connecter_controller.php';

  if (isset($_POST['update'])) {
    $joueurs = get_all_participant($_GET['id']);
    while ($joueur = $joueurs->fetch()){
      if(!empty($_POST['PV_actuel'.$joueur['id_joueurs']])){
        update_caracter_pv_initiative($_POST['PV_actuel'.$joueur['id_joueurs']], $joueur['id_joueurs'],$_POST['initiative'.$joueur['id_joueurs']] );
      }
    }
    $monstres = select_monster_fight($_GET['id']);
    while($monstre = $monstres->fetch()){
      update_monstre($_POST['PV_actuel'.$monstre['id']],$_POST['divers'.$monstre['id']], $monstre['id']);
    }
  }elseif (isset($_POST['initiative'])) {
    $monstres = select_monster_fight($_GET['id']);
    while($monstre = $monstres->fetch()){
      $modele_monstre = get_pattern($monstre['id_modeles']);
      update_initiative_monstre($monstre['id'],$modele_monstre['bonus_initiative']+rand(1,20));
    }
  }
  if(isset($_GET['ordre'])){
    header("Location: ../un_combat?id=".$_GET['id']."&ordre=".$_GET['ordre']);
  }
  else {
    header("Location: ../un_combat?id=".$_GET['id']);
  }
 ?>
