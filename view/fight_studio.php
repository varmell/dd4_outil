<html>
  <head>
    <meta charset="utf-8">
    <?php include 'config.php'; ?>
    <link rel="icon" type="image/png" href="<?php echo $domaine; ?>ressources/logo.png" />
    <title>DD4-Combats</title>
  </head>
  <body>
    <div>
      <?php
      include 'header.php';
      include '../modele/fonction_fight.php';
      include '../modele/fonction_battle.php';
      $nom_combat = get_battle_name($_GET['id']);
      ?>
      <script type="text/javascript" src="ressources/js/div_masquer.js"></script>
    </div>
    <?php include 'nav.php'; ?>
    <div id="studio_combat">
      <br>
      <center><h2><?php echo $nom_combat['nom']; ?></h2></center>
      <center>
        <form action="<?php echo $domaine; ?>controller/fight_studio_controller.php?id=<?php echo $_GET['id']; if(isset($_GET['ordre'])) echo "&ordre=".$_GET['ordre'];?>" method="post">
        <table>
          <tr>
            <th>Nom du joueur</th>
            <th>PV_actuel</th>
            <th>PV_max</th>
            <th>CA</th>
            <th>Initiative</th>
            <th>Réflexes</th>
            <th>Vigueur</th>
            <th>Volonté</th>
            <?php
              if(isset($_GET['id'])){
                $info_joueurs= get_infos_joueur_battle($_GET['id']);
              }
              else {
                header("Location: accueil");
              }
             ?>
          </tr>
          <tr>
            <?php $i=0;
            while ($info_joueur = $info_joueurs->fetch()) {
              ?>
              <tr>
                <td><?php echo $info_joueur['nom_joueur']; ?></td>
                <td><?php echo "<input type='number' name='PV_actuel".$info_joueur['id_joueur']."' value='".$info_joueur['PV_actuel']."' />"; ?></td>
                <td><?php echo $info_joueur['PV_max']; ?></td>
                <td><?php echo $info_joueur['CA']; ?></td>
                <td><?php echo "<input type='number' name='initiative".$info_joueur['id_joueur']."' value='".$info_joueur['initiative']."' />"; ?></td>
                <td><?php echo $info_joueur['reflexes']; ?></td>
                <td><?php echo $info_joueur['vigueur']; ?></td>
                <td><?php echo $info_joueur['volonte']; ?></td>
              </tr>
              <?php
              $participants[$i]=$info_joueur;
              $i++;
            }
            ?>
          </tr>
        </table>
        <br>
        <table>
          <tr>
            <th>Nom du monstre</th>
            <th>ID</th>
            <th>PV_actuel</th>
            <th>PV_max</th>
            <th>CA</th>
            <th>Initiative</th>
            <th>Réflexes</th>
            <th>Vigueur</th>
            <th>Volonté</th>
            <th>VD</th>
            <th>Résistance</th>
            <th>Sorts</th>
            <th>Description</th>
            <th>Divers</th>
            <?php
              if(isset($_GET['id'])){
                $info_monstres= get_infos_monstre_battle($_GET['id']);
              }
              else {
                header("Location: accueil");
              }
             ?>
          </tr>
          <tr>
            <?php $all_id_modele= array();
            while ($info_monstre = $info_monstres->fetch()) {
              ?>
              <tr>
                <td><?php echo $info_monstre['nom_modele']; ?></td>
                <td><?php echo $info_monstre['id']; ?></td>
                <td><?php echo "<input type='number' class='test' min='0' name='PV_actuel".$info_monstre['id']."' value='".$info_monstre['PV_actuel']."' />"; ?></td>
                <td><p title="pv max"><?php echo $info_monstre['PV_max']; ?></p></td>
                <td><p title="CA"><?php echo $info_monstre['CA']; ?></p></td>
                <td><p title="initiative"><?php echo $info_monstre['initiative']; ?></p></td>
                <td><p title="réflexes"><?php echo $info_monstre['reflexes']; ?></p></td>
                <td><p title="vigueur"><?php echo $info_monstre['vigueur']; ?></p></td>
                <td><p title="volonté"><?php echo $info_monstre['volonte']; ?></p></td>
                <td><p title="VD"><?php echo $info_monstre['VD']; ?></p></td>
                <td><?php echo $info_monstre['resistance']; ?></td>
                <td>
                    <button type="button" onclick="toggle_div(this,'<?php echo $info_monstre['id']; ?>');">Afficher</button>
                    <!-- Un div caché avec un attribut id -->
                    <div id="<?php echo $info_monstre['id']; ?>" style="display:none;">
                      <?php echo $info_monstre['description_sorts']?>
                    </div>
                </td>
                <td><?php if(!in_array($info_monstre['id_modeles'],$all_id_modele))echo $info_monstre['description']; ?></td>
                <td><?php echo "<input type='text' name='divers".$info_monstre['id']."' value='".$info_monstre['divers']."' />"; ?></td>
              </tr>
              <?php
              $all_id_modele[]=$info_monstre['id_modeles'];
              $participants[$i]=$info_monstre;
              $i++;
            }
            ?>
          </tr>
        </table>
        <br>
        <?php
         $monstre_test = first_infos_monstre_battle($_GET['id']);
          if($monstre_test['bonus_initiative']==$monstre_test['initiative']){
            echo "<input type='submit' name='initiative' value='Lancer initiative'/>";
          }
          else{
            $ordre=1;
            if(isset($_GET['ordre'])){
              if($_GET['ordre']==1)
              {
                $ordre = 0;
              }else{
                $ordre = 1;
              }
            }
            echo "<a href='un_combat.php?id=".$_GET['id']."&ordre=".$ordre."'>Ordre de tour</a>";
          }
         ?>
          <input type="submit" name="update" value="Mettre à jour"/>
        </form>
        <?php
          if (isset($_GET['ordre']) && $_GET['ordre']==1) {
            $order_participants = order($participants);
            ?>
            <br />
            <table>
            <tr>
              <td>Ordre</td>
              <td>Nom</td>
            </tr>
            <tr>
            <?php foreach ($order_participants as $participant) {
            ?>
              <tr>
                <td><?php echo $participant['initiative']; ?></td>
                <td><?php if(isset($participant['nom_modele'])) echo $participant['nom_modele']; else echo $participant['nom_joueur']; ?></td>
              </tr>
            <?php
            }
            ?>
            </table>
            <?php
          }
         ?>
      </center>
    </div>
  </body>
</html>
