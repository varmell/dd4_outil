<html>
  <div id="menu">
    <ul>
      <li><a class="without_deco" href="accueil"><h3>Accueil</h3></a></li>
      <li><a class="without_deco" href="personnage"><h3>Personnages</h3></a></li>
      <li><a class="without_deco" href="modele_monstre"><h3>Modèles de monstres</h3></a></li>
      <li><a class="without_deco" href="combat"><h3>Créer un combat</h3></a></li>
      <?php if(empty($_SESSION["login"])) {
         echo "<li><a class='without_deco' href='connexion'><h3>Se connecter</h3></a></li>";
       } else
         echo "<li><a class='without_deco' href='controller/deconnection_controller.php'><h3>Déconnexion</h3></a></li>";
        ?>
    </ul>
  </div>
</html>
